package loanCalc;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {
	@FXML
	private Button btnCalcular;
	@FXML
	private TextField txtTIA;
	@FXML
	private TextField txtPlazo;
	@FXML
	private TextField txtMonto;
	@FXML
	private TextField txtCuota;
	@FXML
	private TextField txtTotalDevolver;
		
	
	@FXML
	private void initialize() {
		txtTIA.requestFocus();
	 	
	    btnCalcular.setOnAction(e -> {
	    		calcularPagoPrestamo();   		
	    		
	    });
	        
	        
	    //    btnSalir.setOnAction(e -> {
	    //    	Platform.exit();
	    //    });
	        
	     	
	} // initialize
	
	private void calcularPagoPrestamo() {
		// Tomo los valores de los campos de texto
		double interes = Double.parseDouble(txtTIA.getText());
		int plazo = Integer.parseInt(txtPlazo.getText());
		double monto = Double.parseDouble(txtMonto.getText()); 
		
		// Creo un objeto "Pr�stamo"
		Prestamo prestamo = new Prestamo(interes, plazo, monto);
		// Muestro el Pago Mensual y el Total a Devolver
		txtCuota.setText(String.format("$%.2f", prestamo.getPagoMensual()));
		txtTotalDevolver.setText(String.format("$%.2f", prestamo.getPagoTotal()));
	}
}