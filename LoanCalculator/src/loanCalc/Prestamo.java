package loanCalc;

public class Prestamo {
	private double tasaInteresAnual;
    private int plazoEnA�os;
	private double montoPrestamo;
	/**
	 * LocalDate/LocalTime and LocalDateTime 
	 * classes simplify the development where 
	 * timezones are not required.
	 */
	private java.time.LocalDate fechaPrestamo;

	  /** Constructor por defecto*/
	public Prestamo() {
	    this(2.5, 1, 1000);
	  }

	  /** Constructor para un pr�stamo con una
	   *  Tasa de Inter�s Anual, Plazo en A�os
	      y Monto del Pr�stamo 
	  */
	public Prestamo(double tasaInteresAnual, int plazoEnA�os, double montoPrestamo) {
	    this.tasaInteresAnual = tasaInteresAnual;
	    this.plazoEnA�os = plazoEnA�os;
	    this.montoPrestamo = montoPrestamo;
	   // LocalDate fechaPrestamo = LocalDate.now(); 
	  }

	  /** Devuelve (Return) la Tasa de Inter�s Anual */
	public double getTasaInteresAnual() {
	    return tasaInteresAnual;
	  }

	  /** Fija (Set) una nueva Tasa de Inter�s Anual */
	public void setTasaInteresAnual(double tasaInteresAnual) {
	    this.tasaInteresAnual = tasaInteresAnual;
	  }

	  /** Devuelve (Return) el Plazo en A�os */
	public int getPlazoEnA�os() {
	    return plazoEnA�os;
	  }

	  /** Fija (Set) un nuevo Plazo en A�os */
	public void setPlazoEnA�os(int plazoEnA�os) {
	    this.plazoEnA�os = plazoEnA�os;
	  }

	  /** Devuelve (Return) el Monto del Pr�stamo */
	public double getMontoPrestamo() {
	    return montoPrestamo;
	  }

	  /** Fija (Set) un nuevo Monto de Pr�stamo */
	public void setMontoPrestamo(double montoPrestamo) {
	    this.montoPrestamo = montoPrestamo;
	  }

	  /** Calcula y devuelve (Return) el Pago Mensual */
	public double getPagoMensual() {
	    double tasaInteresMensual = tasaInteresAnual / 1200;
	    double pagoMensual = montoPrestamo * tasaInteresMensual / (1 -
	      (Math.pow(1 / (1 + tasaInteresMensual), plazoEnA�os * 12)));
	    return pagoMensual;    
	  }

	  /** Calcula y devuelve (Return) el Pago Total */
	public double getPagoTotal() {
	    double pagoTotal = getPagoMensual() * plazoEnA�os * 12;
	    return pagoTotal;    
	  }

	  /** Devuelve (Return) la fecha del pr�stamo */
	public java.time.LocalDate getFechaPrestamo() {
	    return fechaPrestamo;
	  }
}